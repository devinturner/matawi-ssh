package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/devinturner/matawi-ssh/pkg/session"
	"gitlab.com/devinturner/matawi-ssh/pkg/util"
)

var hostsFlag string
var username string
var command string
var key string
var timeout int

func main() {
	flag.StringVar(&hostsFlag, "hosts", "", "comma-separated list of host(s) to connect to via ssh")
	flag.StringVar(&username, "user", "", "ssh username")
	flag.StringVar(&key, "key", "", "path to ssh private key")
	flag.StringVar(&command, "c", "whoami", "command to execute on remote machine(s)")
	flag.IntVar(&timeout, "timeout", 30, "ssh timeout in seconds")
	flag.Parse()

	hosts, err := util.ParseHostList(os.Stdin, hostsFlag)
	if err != nil {
		log.Fatal(err)
	}

	auth, err := session.NewAuth(username, key)
	if err != nil {
		log.Fatal(err)
	}

	config := session.Configure(auth, timeout)
	resChan := make(chan *session.Response)
	defer close(resChan)
	for _, host := range hosts {
		go func(host string) {
			resChan <- session.Execute(host, config, command)
		}(host)
	}

	for i := 0; i < len(hosts); i++ {
		select {
		case res := <-resChan:
			scanner := bufio.NewScanner(bytes.NewReader(res.Data))
			scanner.Split(bufio.ScanLines)
			for scanner.Scan() {
				fmt.Printf("%v [%v] | %v\n", res.Host, time.Now().UTC(), scanner.Text())
			}
		}
	}
}
