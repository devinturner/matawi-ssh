GOBUILD=go build
GOCLEAN=go clean
GOTEST=go test
GOMOD=go mod
BINARY_NAME=matawi-ssh
LDFLAGS= -w\
				 -s

all: clean test build

build:
	$(GOBUILD) -o $(BINARY_NAME) -ldflags="$(LDFLAGS)"

test:
	$(GOTEST) -v ./...

clean:
	$(GOCLEAN)
	rm -f $(BINARY_NAME)
	$(GOMOD) tidy

run:
	$(GOBUILD) -o $(BINARY_NAME) -v ./...
