module gitlab.com/devinturner/matawi-ssh

go 1.12

require (
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71
)
