package util

import (
	"encoding/csv"
	"io"
	"os"
	"strings"

	"golang.org/x/crypto/ssh/terminal"
)

func ParseHostList(stdin *os.File, hostString string) ([]string, error) {
	if terminal.IsTerminal(int(stdin.Fd())) {
		return csvToStringSlice(strings.NewReader(removeSpaces(hostString)))
	}
	return csvToStringSlice(stdin)
}

func csvToStringSlice(r io.Reader) ([]string, error) {
	out, err := csv.NewReader(r).Read()
	if err != nil {
		return nil, err
	}
	return deleteEmpty(out), nil
}

func removeSpaces(str string) string {
	return strings.Join(strings.Fields(str), "")
}

func deleteEmpty(l []string) []string {
	var out []string
	for _, i := range l {
		if i != "" {
			out = append(out, i)
		}
	}
	return out
}
