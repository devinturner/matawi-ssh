package util

import (
	"bytes"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCsvToStringSlice(t *testing.T) {
	cases := []struct {
		name        string
		in          io.Reader
		out         []string
		errExpected bool
	}{
		{"empty input returns error", bytes.NewBufferString(""), nil, true},
		{"single input passes", bytes.NewBufferString("a"), []string{"a"}, false},
		{"multi input passes", bytes.NewBufferString("a,b,c"), []string{"a", "b", "c"}, false},
		{"multi input w/ spaces passes", bytes.NewBufferString(removeSpaces("a ,    b     ,c  ")), []string{"a", "b", "c"}, false},
		{
			"newlines are ignored and passes",
			bytes.NewBufferString(`a,b,
			c`),
			[]string{"a", "b"},
			false,
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			actual, err := csvToStringSlice(tt.in)
			if tt.errExpected {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
			}
			assert.ElementsMatch(t, tt.out, actual)
		})
	}
}
