package session

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"syscall"
	"time"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/terminal"
)

type Auth struct {
	Username string
	Methods  []ssh.AuthMethod
}

func getPublicKeys(keyPath string) (ssh.AuthMethod, error) {
	fmt.Printf("Enter ssh private key password: ")
	passBytes, err := terminal.ReadPassword(int(syscall.Stdin))
	fmt.Println()
	if err != nil {
		return nil, err
	}

	keyBytes, err := ioutil.ReadFile(keyPath)
	if err != nil {
		return nil, err
	}

	signer, err := ssh.ParsePrivateKeyWithPassphrase(keyBytes, passBytes)
	if err != nil {
		return nil, err
	}
	return ssh.PublicKeys(signer), nil
}

func NewAuth(username, key string) (*Auth, error) {
	if username == "" {
		return nil, fmt.Errorf("username must be specified")
	}

	var methods []ssh.AuthMethod
	if key != "" {
		k, err := getPublicKeys(key)
		if err != nil {
			return nil, err
		}
		methods = append(methods, k)
	}
	return &Auth{Username: username, Methods: methods}, nil
}

type Response struct {
	Host string
	Data []byte
}

func Configure(auth *Auth, timeout int) *ssh.ClientConfig {
	return createClientConfig(auth, timeout)
}

func createClientConfig(auth *Auth, timeout int) *ssh.ClientConfig {
	return &ssh.ClientConfig{
		User:            auth.Username,
		Auth:            auth.Methods,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         time.Duration(timeout) * time.Second,
	}
}

func Execute(host string, config *ssh.ClientConfig, command string) *Response {
	c, err := ssh.Dial("tcp", host, config)
	if err != nil {
		return &Response{host, []byte(fmt.Sprintf("Failed to dial: %v", err))}
	}

	s, err := c.NewSession()
	if err != nil {
		return &Response{host, []byte(fmt.Sprintf("Failed to create session: %v", err))}
	}
	defer s.Close()

	var stdout bytes.Buffer
	s.Stdout = &stdout
	if err := s.Run(command); err != nil {
		return &Response{host, []byte(err.Error())}
	}
	return &Response{host, stdout.Bytes()}
}
